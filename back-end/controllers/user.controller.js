const db = require('../models/');
const User = db.users; 


exports.save = async (req, res)=>{

    if(!req.body)
    {
        return res.send({message:"Please provide the form data"})
    }
    const user  = new User({
        name:req.body.name,
        email:req.body.email,
        hightset_education:req.body.hightset_education,
        address: req.body.address,
        hobbies:req.body.hobbies


    });
    user
    .save(user)
    .then(data=>{
         res.status(201).send("information saved successfully");
    })
    .catch(err=>{
        res.status(500).send({
            message:"Something went wrong"
        })
    })


}
exports.get = async(req,res)=>{
    
    User.find({},function(err,data){
        if(data)
        {
            res.send(data);
        }
    })

        // await User.find({}).then(data=>{res.status(200).send(data)}).catch(err=>{
        //     res.status(500).send({
        //         message:"something went wrong"
        //     })
        // })  
}

exports.update = (req, res)=>{
        if(!req.body)
        {
            res.status(400).send({
                message:"data update cannot be empty"
            })
        }
        const id  = req.params.id;
        User.findIdAndUpdate(id,req.body).then(data=>{
            if(!data)
            {
                res.status(404).send({
                    message:`Cam't update User with id ${id}`
                })
            }
            else
            {
                res.status(200).send({message:"User data updated successfully"})
            }
        })
}
exports.delete =(req, res)=>{
const id = req.params.id;
User.findByIdAndRemove(id).then(data=>{
    if(!data)
    {
        res.status(404).send({message:`User of ${id} cannot be deleted `})
    }
    else{
        res.status(200).send({message:"User deleted successcfully "})
    }
}).catch(err=>{
    res.status(500).send({
        message:"Something went wrong"
    })
})

}
const express = require("express");
const router = express.Router();
const mongoose = require('mongoose');
const userController = require('../controllers/user.controller');
const db = require('../models');
db.mongoose.connect(db.url, {
  useNewUrlParser:true,
  useUnifiedTopology:true
}).then(()=>console.log("connected")).catch(err=>{
  console.log("error");
  process.exit();
})

/* GET home page. */
router.get("/", (req, res) => {
  res.render("index", { title: "Express" });
});
router.get("/test", (req, res) => {
  res.send("hello");
});
router.post('/save/user',userController.save);
router.get('/get/user',userController.get);
router.delete('/delete',userController.delete);
router.put('/upate/user',userController.update);



module.exports = router;

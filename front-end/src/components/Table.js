import axios from 'axios';
import React,{useEffect, useState} from 'react'



export default function Table() {
    const [user, setUserData] = useState([]);
    useEffect(() => {
axios.get("http://localhost:3001/get/user")
.then((data)=>{
    
    setUserData(data.data);
})
    },[])

    
    return (
        <div>
            <Table >
  <thead>
    <tr>
      
      <th> Name</th>
      <th> Email</th>
      <th> Hobbies</th>
      <th> Education</th>
      <th> Address</th>
    </tr>
  </thead>
  <tbody>
      {user.map((data)=>{
          return(
        <tr>
        <td>{data.name}</td>
          <td>{data.email}</td>
          <td>{data.hobbies}</td>
          <td>{data.highest_education}</td>
          <td>{data.address}</td>
        
        </tr>   
          )

      })}
   
  
  </tbody>
</Table>
        </div>
    )
}

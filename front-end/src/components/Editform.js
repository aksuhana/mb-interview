import React ,{useState} from 'react'
import axios from 'axios';
export default function Editform() {
    const [name, setName]=useState("");
    const [hobbies, setHobbies]=useState("");
    const [email, setEmail]=useState("");
    const [education, setEducation]=useState("");

    const url ="localhost:3001";
    const formSubmit =() =>{
        axios.put(url+'/update/user', {
            name,
            hobbies,
            email,
            education


        }).then(()=>{

            //redirectPath.push('/read');
        });

    }


    return (


        <>
        <div>
        <form onSubmit ={formSubmit}>
            <div>
            <lable  htmlFor ="name">
            
            Enter Name
            </lable>
            <input type = "text" placeholder="name" onChange={e=>setName(e.target.value)} value={name} ></input>
            </div>
            <div>
            <lable  htmlFor ="email">
            
            Enter Email
            </lable>
            <input type = "text" placeholder="email" onChange={e=>setEmail(e.target.value)} value={email} ></input>
            </div>
            <div>
            <lable  htmlFor ="hobbies">
            
            Enter Hobbies
            </lable>
            <input type = "text" placeholder="hobbies" onChange={e=>setHobbies(e.target.value)} value={hobbies}> </input>
            </div>
            <div>
            <lable  htmlFor ="Hightest education">
            
            Enter Hightest Eductation
            </lable>
            <input type = "text" placeholder="hightest education" onChange={e=>setEducation(e.target.value)} value={education}></input>
            </div>
            
        </form> 

        </div>
        </>
       
    )
}
